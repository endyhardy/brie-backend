<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Auth::routes();
// Route::group(['middleware' => 'auth:api'], function () {
    Route::resource('/comments', 'CommentController')->only(['index', 'store']);
    Route::resource('/categories', 'CategoryController')->only(['index', 'show']);
    Route::get('/feed', 'FeedController@index');
// });

