<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Model
{
    use SoftDeletes, Sluggable;

    protected $dates = [
        'deleted_at'
    ];

    public function sluggable() {
        return ['slug' => ['source' => 'name']];
    }

    public function parent() {
        return $this->belongsTo('App\Category', 'parent_id');
    }

    public function showcases() {
        return $this->morphedByMany('App\Showcase', 'categorizable');
    }

    public function vendors() {
        return $this->morphedByMany('App\Vendor', 'categorizable');
    }

    public function getRouteKeyName() {
        return 'slug';
    }

}
