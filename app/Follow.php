<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{

    public function followable() {
        return $this->morphTo();
    }

    public function follower() {
        return $this->belongsTo('App\User');
    }

}
