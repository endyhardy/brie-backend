<?php

namespace App\Transformers;

use App\Vendor;
use App\Transformers\UserTransformer;
use League\Fractal\TransformerAbstract;

class VendorTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'user'
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Vendor $vendor)
    {
        return [
            'bio' => $vendor->bio,
            'geolocation' => $vendor->geo_lat && $vendor->geo_long ?
                [
                    'latitude' => $vendor->geo_lat,
                    'longitude' => $vendor->geo_long,
                ] : null,
            'address' => $vendor->address,
            'web' => $vendor->web,
            'reviews' => [
                'count' => $vendor->reviews_count,
                'score' => $vendor->review_score,
            ],
        ];
    }

    public function includeUser(Vendor $vendor) {
        return $this->item($vendor->user, new UserTransformer);
    }

}
