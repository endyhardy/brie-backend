<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class SelfUserTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'vendor'
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'email' => $user->email,
            'name' => $user->name,
            'two_step_verification_enabled' => $user->enable_2sv,
            'phone' => $user->phone,
            'birth' => optional($user->birth->format('j M Y')),
            'gender' => $user->gender ? ($user->gender == 'm' ? 'Male' : 'Female') : null,
        ];
    }

    public function includeVendor(User $user)
    {
        return $this->item($user->vendor, new VendorTransformer);
    }
}
