<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class CommentTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(\App\Comment $comment)
    {
        return [
            'message' => $comment->message,
            'user_name' => $comment->user->name,
            'user_handle' => $comment->user->handle,
            'created_at' => $comment->created_at->diffForHumans(),
        ];
    }
}
