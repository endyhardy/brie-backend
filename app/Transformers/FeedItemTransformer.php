<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class FeedItemTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'user'
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($feedItem)
    {
        $images = collect([]);
        for ($i = 0; $i < rand(1, 10); $i++) {
            $imageDimensions = [
                'width' => rand(200, 300),
                'height' => rand(200, 300),
            ];
            $images->push([
                'uri' => 'https://placeimg.com/'. $imageDimensions['width'] . '/' . $imageDimensions['height'] .'/any',
                'width' => $imageDimensions['width'],
                'height' => $imageDimensions['height'],
            ]);
        }
        return [
            'id' => $feedItem->id,
            'created_at' => datediff_simplify($feedItem->created_at),
            'images' => $images->toArray(),
            'category' => $feedItem->categories[0]->name,
            'message' => $feedItem->message,
            'liked' => $feedItem->likes->contains('user_id', auth()->user()->id),
            'likes_count' => $feedItem->likes_count,
            'comments_count' => $feedItem->comments_count,
        ];
    }

    public function includeUser($feedItem) {
        return $this->item($feedItem->vendor->user, new UserTransformer);
    }
}
