<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Showcase extends Model
{

    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    public function categories() {
        return $this->morphToMany('App\Category', 'categorizable');
    }

    public function comments() {
        return $this->morphMany('App\Comment', 'commentable');
    }

    public function likes() {
        return $this->morphMany('App\Like', 'likeable');
    }

    public function vendor() {
        return $this->belongsTo('App\Vendor');
    }

}
