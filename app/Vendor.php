<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendor extends Model
{

    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    public function categories() {
        return $this->morphToMany('App\Category', 'categorizable');
    }

    public function follows() {
        return $this->morphMany('App\Follow', 'followable');
    }

    public function reviews() {
        return $this->hasMany('App\Review');
    }

    public function showcases() {
        return $this->hasMany('App\Showcase');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function getReviewScoreAttribute() {
        return $this->reviews()->avg('score');
    }

}
