<?php

if ( !function_exists('datediff_simplify') ) {

  function datediff_simplify($date) {
    if ( $date->gt(\Carbon\Carbon::now()->subDay()) )
      return $date->diffInHours() . 'h';

    if ( $date->gt(\Carbon\Carbon::now()->subMonth()) )
      return $date->diffInDays() . 'd';

    if ( $date->gt(\Carbon\Carbon::now()->subYear()) )
      return $date->diffInMonths() . 'mo';

    return $date->diffInYears() . 'y';
  }

}