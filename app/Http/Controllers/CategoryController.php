<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Transformers\VendorTransformer;
use App\Transformers\CategoryTransformer;
use App\Transformers\FeedItemTransformer;

class CategoryController extends Controller
{

    public function index() {
        $categories = Category::has('vendors')->orderBy('name')->get();

        return fractal()
            ->collection($categories)
            ->transformWith(new CategoryTransformer)
            ->respond();
    }

    public function show(Category $category) {

        auth()->loginUsingId(\App\User::inRandomOrder()->first()->id); // use real user next time

        $feed = $category->showcases()
            ->with(['likes', 'vendor.user'])
            ->withCount(['likes', 'comments'])
            ->orderBy('created_at', 'desc')
            ->take(5)
            ->get();

        $vendors = $category->vendors()
            ->with(['user', 'reviews'])
            ->withCount('reviews')
            ->get()
            ->sortByDesc('review_score')
            ->take(3)
            ->values();

        return response()->json([
            'feed' => fractal()->collection($feed)->transformWith(new FeedItemTransformer)->includeUser()->toArray(),
            'vendors' => fractal()->collection($vendors)->transformWith(new VendorTransformer)->includeUser()->toArray(),
        ]);
    }

}
