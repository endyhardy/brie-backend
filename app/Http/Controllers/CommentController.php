<?php

namespace App\Http\Controllers;

use Validator;
use App\Comment;
use Illuminate\Http\Request;
use App\Transformers\CommentTransformer;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $comments = Comment::orderBy('created_at', 'desc')
            ->with('user');
        if ($request->showcase_id) {
            $comments->where('commentable_id', $request->showcase_id)->where('commentable_type', 'App\Showcase');
        }

        return fractal()
            ->collection($comments->get())
            ->transformWith(new CommentTransformer)
            ->respond();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->toArray(), [
            'message' => 'required|string',
            'commentable_type' => 'required|in:showcase',
        ]);
        $validator->sometimes('commentable_id', 'required|exists:showcases', function ($input) {
            return $input->commentable_type = 'showcase';
        });

        switch($request->commentable_type) {
            case 'showcase': $commentable = \App\Showcase::find($request->commentable_id); break;
        }

        $comment = new Comment;
        $comment->message = $request->message;
        $comment->user_id = $request->user()->id;
        $comment = $commentable->comments()->save($comment);

        return fractal()
            ->item($comment)
            ->transformWith(new CommentTransformer)
            ->respond();
    }

}
