<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class FeedController extends Controller
{

    public function index() {
        auth()->loginUsingId(\App\User::inRandomOrder()->first()->id); // use real user next time

        if ($user = auth()->user()) {
            $followedVendors = $user->follows()->where('followable_type', 'App\Vendor')->get();
            $paginator = \App\Showcase::whereIn('vendor_id', $followedVendors->pluck('followable_id'));
        }

        $paginator = $paginator
            ->has('categories')
            ->orderBy('created_at', 'desc')
            ->with('categories', 'vendor.user', 'likes')
            ->withCount(['likes', 'comments'])
            ->paginate(20);
        $showcases = $paginator->getCollection();

        return fractal()
            ->collection($showcases)
            ->transformWith(new \App\Transformers\FeedItemTransformer)
            ->includeUser()
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->respond();
    }

}
