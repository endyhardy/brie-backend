<?php

use Faker\Generator as Faker;

$factory->define(App\Showcase::class, function (Faker $faker) {
    return [
        'message' => $faker->sentence,
        'commentable' => $faker->boolean(90),
        'created_at' => $faker->dateTimeThisMonth,
    ];
});
