<?php

use Faker\Generator as Faker;

$factory->define(App\Vendor::class, function (Faker $faker) {
    $hasLocation = $faker->boolean(75);

    return [
        'bio' => $faker->paragraph,
        'geo_lat' => $hasLocation ? $faker->latitude : null,
        'geo_long' => $hasLocation ? $faker->longitude : null,
        'address' => $faker->address,
        'web' => $faker->boolean ? 'https://' . $faker->domainName : null,
    ];
});
