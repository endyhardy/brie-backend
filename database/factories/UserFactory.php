<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    $phone = $faker->boolean ? $faker->e164PhoneNumber : null;
    return [
        'name' => $faker->unique()->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
        'birth' => $faker->boolean(30) ? $faker->date('Y-m-d', '-16 years') : null,
        'gender' => $faker->boolean(30) ? (rand(0, 1) ? 'male' : 'female') : null,
        'phone' => $phone,
        'enable_2sv' => $phone ? $faker->boolean(50) : false,
    ];
});
