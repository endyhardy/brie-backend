<?php

use Faker\Generator as Faker;

$factory->define(App\Review::class, function (Faker $faker) {
    return [
        'score' => rand(1, 5),
        'message' => $faker->sentence
    ];
});
