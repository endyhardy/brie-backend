<?php

use Illuminate\Database\Seeder;
use Faker\Factory;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->truncate();
        DB::table('categorizables')->truncate();

        $faker = Factory::create();
        $categories = collect([
            [ 'name' => 'Event Organizer', 'description' => $faker->sentence ],
            [ 'name' => 'Bridal', 'description' => $faker->sentence ],
            [ 'name' => 'Catering', 'description' => $faker->sentence ],
            [ 'name' => 'Entertainment', 'description' => $faker->sentence ],
            [ 'name' => 'Culture', 'description' => $faker->sentence ],
            [ 'name' => 'Rental', 'description' => $faker->sentence ],
            [ 'name' => 'Photography', 'description' => $faker->sentence ],
            [ 'name' => 'Decoration', 'description' => $faker->sentence ],
            [ 'name' => 'Printing', 'description' => $faker->sentence ],
            [ 'name' => 'Souvenir', 'description' => $faker->sentence ],
            [ 'name' => 'Tailor', 'description' => $faker->sentence ],
            [ 'name' => 'Venue', 'description' => $faker->sentence ],
            [ 'name' => 'Hair & Make-Up', 'description' => $faker->sentence ],
        ]);
        $categories->each(function ($cat) {
            App\Category::create($cat);
        });
    }
}
