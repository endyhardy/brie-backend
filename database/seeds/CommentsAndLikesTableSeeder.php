<?php

use Illuminate\Database\Seeder;

class CommentsAndLikesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('likes')->truncate();
        DB::table('comments')->truncate();

        $users = App\User::all();
        App\Showcase::all()->each(function ($showcase) use ($users) {
            factory(App\Comment::class, rand(0, 20))
                ->make()
                ->each(function ($comment) use ($showcase, $users) {
                    $comment->user_id = $users->random()->id;
                    $showcase->comments()->create($comment->toArray());
                });

            $users->random(rand(0, ceil($users->count() / 10)))->each(function ($user) use ($showcase) {
                $showcase->likes()->create(['user_id' => $user->id]);
            });
        });
    }
}
