<?php

use Illuminate\Database\Seeder;

class ReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reviews')->truncate();

        $users = App\User::all();

        App\Vendor::all()->each(function ($vendor) use ($users) {
            $reviews = factory(App\Review::class, rand(0, 10))->make();
            $reviews->each(function ($review) use ($vendor, $users) {
                $review->user_id = $users->random()->id;
                $vendor->reviews()->save($review);
            });
        });

    }
}
