<?php

use Illuminate\Database\Seeder;

class VendorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vendors')->truncate();
        DB::table('follows')->truncate();
        DB::table('showcases')->truncate();

        $users = App\User::all();
        $users = $users->random(ceil($users->count() * 0.2));
        $vendors = factory(App\Vendor::class, $users->count())->make();
        $categories = App\Category::all();

        $users->each(function ($user, $index) use ($vendors, $categories)
        {
            $categories = $categories->random(rand(1, 5));
            $vendor = $user->vendor()->create($vendors[$index]->toArray());
            $vendor->categories()->sync($categories);
            $vendor->showcases()->saveMany(factory(App\Showcase::class, rand(5, 30))->make())
                ->each(function ($showcase) use ($categories) {
                    $showcase->categories()->attach($categories->random());
                });

            $followers = App\User::where('id', '<>', $user->id)->get()->random(rand(1, App\User::count() - 1));
            $followers->each(function ($follower) use ($vendor) {
                $vendor->follows()->create(['user_id' => $follower->id]);
            });
        });

    }
}
